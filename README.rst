Google ('googl')
****************

Documentation
https://www.kbsoftware.co.uk/docs/app-googl.html

Install
=======

Virtual Environment
-------------------

::

  python3 -m venv venv-googl
  source venv-googl/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Release
=======

https://www.kbsoftware.co.uk/docs/
