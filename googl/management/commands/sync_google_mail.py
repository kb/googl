# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from googl.models import GoogleMail


class Command(BaseCommand):

    help = "Sync Google mail #1856"

    def handle(self, *args, **options):
        self.stdout.write(self.help)
        count = GoogleMail.objects.sync()
        self.stdout.write(
            "{} - Complete.  Downloaded {} Google mail messages".format(
                self.help, count
            )
        )
