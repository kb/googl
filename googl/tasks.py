# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings

from googl.models import GoogleMail


logger = logging.getLogger(__name__)


# https://dramatiq.io/cookbook.html#binding-worker-groups-to-queues
@dramatiq.actor(queue_name=settings.DRAMATIQ_QUEUE_NAME)
def sync_google_email():
    logger.info("Sync Google mail #1856")
    count = GoogleMail.objects.sync()
    logger.info("Sync Google mail.  {} emails - complete".format(count))
    return count
