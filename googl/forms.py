# -*- encoding: utf-8 -*-
from django import forms

from .models import EmailAddressToIgnore


class EmailAddressToIgnoreEmptyForm(forms.ModelForm):
    class Meta:
        model = EmailAddressToIgnore
        fields = ()


class EmailAddressToIgnoreForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        f = self.fields["email"]
        f.widget.attrs.update({"class": "pure-input-1"})

    class Meta:
        model = EmailAddressToIgnore
        fields = ("email",)
