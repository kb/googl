# -*- encoding: utf-8 -*-
import pytest

from googl.models import EmailAddressToIgnore
from googl.tests.factories import EmailAddressToIgnoreFactory
from login.tests.factories import UserFactory


@pytest.mark.django_db
def test_ignore():
    EmailAddressToIgnoreFactory(email="a@a.com")
    x = EmailAddressToIgnoreFactory(email="b@b.com")
    x.set_deleted(UserFactory())
    assert EmailAddressToIgnore.objects.ignore("a@a.com") is True
    assert EmailAddressToIgnore.objects.ignore("b@b.com") is False
    assert EmailAddressToIgnore.objects.ignore("x@x.com") is False


@pytest.mark.django_db
def test_ordering():
    EmailAddressToIgnoreFactory(email="a@mail.com")
    EmailAddressToIgnoreFactory(email="b@mail.com")
    assert ["a@mail.com", "b@mail.com"] == [
        x.email for x in EmailAddressToIgnore.objects.all()
    ]


@pytest.mark.django_db
def test_str():
    x = EmailAddressToIgnoreFactory(email="patrick@kbsoftware.co.uk")
    assert "patrick@kbsoftware.co.uk" == str(x)
