# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from login.tests.fixture import perm_check
from .factories import EmailAddressToIgnoreFactory, GoogleMailFactory


@pytest.mark.django_db
def test_email_address_to_ignore_create(perm_check):
    perm_check.staff(reverse("googl.email.address.to.ignore.create"))


@pytest.mark.django_db
def test_email_address_to_ignore_delete(perm_check):
    x = EmailAddressToIgnoreFactory()
    perm_check.staff(
        reverse("googl.email.address.to.ignore.delete.toggle", args=[x.pk])
    )


@pytest.mark.django_db
def test_email_address_to_ignore_list(perm_check):
    perm_check.staff(reverse("googl.email.address.to.ignore.list"))


@pytest.mark.django_db
def test_google_mail_detail(perm_check):
    x = GoogleMailFactory()
    perm_check.staff(reverse("googl.mail.detail", args=[x.pk]))


@pytest.mark.django_db
def test_google_mail_list(perm_check):
    perm_check.staff(reverse("googl.mail.list"))
