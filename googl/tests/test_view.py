# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from googl.models import EmailAddressToIgnore
from googl.tests.factories import EmailAddressToIgnoreFactory
from login.tests.factories import TEST_PASSWORD, UserFactory


@pytest.mark.django_db
def test_email_address_to_ignore_create(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    url = reverse("googl.email.address.to.ignore.create")
    response = client.post(url, {"email": "patrick@kbsoftware.co.uk"})
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("googl.email.address.to.ignore.list") == response.url
    assert 1 == EmailAddressToIgnore.objects.count()
    EmailAddressToIgnore.objects.get(email="patrick@kbsoftware.co.uk")


@pytest.mark.django_db
def test_email_address_to_ignore_delete(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    x = EmailAddressToIgnoreFactory()
    response = client.post(
        reverse("googl.email.address.to.ignore.delete.toggle", args=[x.pk])
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("googl.email.address.to.ignore.list") == response.url
    x.refresh_from_db()
    assert x.is_deleted is True


@pytest.mark.django_db
def test_email_address_to_ignore_delete_toggle(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    x = EmailAddressToIgnoreFactory()
    x.set_deleted(user)
    response = client.post(
        reverse("googl.email.address.to.ignore.delete.toggle", args=[x.pk])
    )
    assert HTTPStatus.FOUND == response.status_code
    assert reverse("googl.email.address.to.ignore.list") == response.url
    x.refresh_from_db()
    assert x.is_deleted is False


@pytest.mark.django_db
def test_email_address_to_ignore_list(client):
    """List of email addresses to ignore.

    Check the deleted items are last in the list.

    """
    user = UserFactory(is_staff=True)
    EmailAddressToIgnoreFactory(email="c@c.com")
    x = EmailAddressToIgnoreFactory(email="b@b.com")
    x.set_deleted(user)
    EmailAddressToIgnoreFactory(email="a@a.com")
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("googl.email.address.to.ignore.list"))
    assert HTTPStatus.OK == response.status_code
    assert 3 == EmailAddressToIgnore.objects.count()
    assert "object_list" in response.context
    assert ["a@a.com", "c@c.com", "b@b.com"] == [
        x.email for x in response.context["object_list"]
    ]
