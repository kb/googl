# -*- encoding: utf-8 -*-
import pytest

from googl.models import GoogleMail
from googl.tests.factories import GoogleMailFactory


@pytest.mark.django_db
def test_exists():
    GoogleMailFactory(msg_id="A123")
    assert GoogleMail.objects.msg_id_exists("A123") is True


@pytest.mark.django_db
def test_exists_not():
    GoogleMailFactory(msg_id="A123")
    assert GoogleMail.objects.msg_id_exists("XYZ") is False


@pytest.mark.django_db
def test_str():
    google_mail = GoogleMailFactory(msg_id="A123")
    assert "A123" == str(google_mail)


def test_strip_multi_newline():
    text = """
        ABC


        DEF
        GHI

        JKL
    """
    expect = """
        ABC

        DEF
        GHI

        JKL
    """
    assert expect == GoogleMail.objects._strip_multi_newlines(text)


@pytest.mark.django_db
def test_summary():
    message = """
        Dear ABC


        DEF
        GHI

        JKL
        MNO

        PQR
    """
    mail = GoogleMailFactory(message=message)
    assert "DEF\nGHI\nJKL\nMNO\n..." == mail.summary()
