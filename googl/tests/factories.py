# -*- encoding: utf-8 -*-
import factory

from django.utils import timezone

from googl.models import EmailAddressToIgnore, GoogleMail


class EmailAddressToIgnoreFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = EmailAddressToIgnore


class GoogleMailFactory(factory.django.DjangoModelFactory):
    email_date = timezone.now()

    class Meta:
        model = GoogleMail

    @factory.sequence
    def msg_id(n):
        return "msg_id_{:02d}".format(n)
