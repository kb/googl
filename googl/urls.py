# -*- encoding: utf-8 -*-
from django.urls import re_path

from googl.views import (
    EmailAddressToIgnoreCreateView,
    EmailAddressToIgnoreDeleteToggleView,
    EmailAddressToIgnoreListView,
    GoogleMailDetailView,
    GoogleMailListView,
)


urlpatterns = [
    re_path(r"^$", view=GoogleMailListView.as_view(), name="googl.mail.list"),
    re_path(
        r"^mail/(?P<pk>\d+)/$",
        view=GoogleMailDetailView.as_view(),
        name="googl.mail.detail",
    ),
    re_path(
        r"^email/address/to/ignore/create/$",
        view=EmailAddressToIgnoreCreateView.as_view(),
        name="googl.email.address.to.ignore.create",
    ),
    re_path(
        r"^email/address/to/ignore/(?P<pk>\d+)/delete/$",
        view=EmailAddressToIgnoreDeleteToggleView.as_view(),
        name="googl.email.address.to.ignore.delete.toggle",
    ),
    re_path(
        r"^email/address/to/ignore/$",
        view=EmailAddressToIgnoreListView.as_view(),
        name="googl.email.address.to.ignore.list",
    ),
]
