# -*- encoding: utf-8 -*-
from rest_framework import serializers

from .models import GoogleMail


class GoogleMailSerializer(serializers.ModelSerializer):
    class Meta:
        model = GoogleMail
        fields = ("from_email",)
