# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin

from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView, DetailView, ListView, UpdateView

from base.view_utils import BaseMixin, RedirectNextMixin
from .forms import EmailAddressToIgnoreEmptyForm, EmailAddressToIgnoreForm
from .models import EmailAddressToIgnore, GoogleMail


class EmailAddressToIgnoreCreateView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, CreateView
):
    form_class = EmailAddressToIgnoreForm
    model = EmailAddressToIgnore

    def get_success_url(self):
        return reverse("googl.email.address.to.ignore.list")


class EmailAddressToIgnoreDeleteToggleView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, UpdateView
):
    form_class = EmailAddressToIgnoreEmptyForm
    model = EmailAddressToIgnore
    template_name = "googl/email_address_to_ignore_delete_toggle_form.html"

    def form_valid(self, form):
        self.object = form.save(commit=False)
        if self.object.is_deleted:
            self.object.undelete()
            caption = "Re-instated"
        else:
            self.object.set_deleted(self.request.user)
            caption = "Deleted"
        messages.info(self.request, "{} {}".format(caption, self.object.email))
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("googl.email.address.to.ignore.list")


class EmailAddressToIgnoreListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    paginate_by = 20

    def get_queryset(self):
        return EmailAddressToIgnore.objects.all().order_by("deleted", "email")


class GoogleMailDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    RedirectNextMixin,
    BaseMixin,
    DetailView,
):

    model = GoogleMail


class GoogleMailListView(
    LoginRequiredMixin, StaffuserRequiredMixin, BaseMixin, ListView
):

    model = GoogleMail
    paginate_by = 20
