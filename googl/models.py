# -*- encoding: utf-8 -*-
import base64
import googleapiclient.discovery
import logging
import pytz

from bs4 import BeautifulSoup
from datetime import datetime
from django.conf import settings
from django.db import models
from email.utils import parseaddr
from google.oauth2 import service_account

from base.model_utils import TimedCreateModifyDeleteModel
from contact.models import Contact


logger = logging.getLogger(__name__)


class GoogleError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


class EmailAddressToIgnoreManager(models.Manager):
    def ignore(self, email):
        result = False
        try:
            x = self.model.objects.get(email=email)
            result = not x.is_deleted
        except self.model.DoesNotExist:
            pass
        return result


class EmailAddressToIgnore(TimedCreateModifyDeleteModel):
    """Do not import these email addresses from Google."""

    email = models.EmailField(unique=True)
    objects = EmailAddressToIgnoreManager()

    class Meta:
        ordering = ["email"]
        verbose_name = "Ignore email address"
        verbose_name_plural = "Ignore email addresses"

    def __str__(self):
        return "{}".format(self.email)


class GoogleMailManager(models.Manager):
    SCOPES = [
        "https://www.googleapis.com/auth/admin.directory.user.readonly",
        "https://www.googleapis.com/auth/gmail.readonly",
    ]

    def _contact_emails(self, user):
        result = []
        try:
            contact = Contact.objects.get(user=user)
            result = [x.email for x in contact.current_emails()]
        except Contact.DoesNotExist:
            pass
        return result

    def _domain_emails(self):
        """Retrieve the email addresses for all the users on the domain.

        .. note:: I decided to create the ``SERVICE_ACCOUNT_USER`` setting
                  because I preferred to specify the email address of the user
                  who is creating the credentials for listing domain users.
                  It might work fine with any user, but I am not sure.

        """
        result = []
        credentials = self._service_account(settings.SERVICE_ACCOUNT_USER)
        service = self._service_admin(credentials)
        results = (
            service.users()
            .list(domain=settings.SERVICE_ACCOUNT_DOMAIN)
            .execute()
        )
        users = results.get("users", [])
        for user in users:
            result.append(user["primaryEmail"])
        return result

    def _mail(self, service, user_id, msg_id):
        """Retrieve the mail message from the API.

        Returns ``True`` if the message was added to the ``GoogleMail`` model.

        - I have commented out ``labelIds``, but this will include ``SENT`` if
          the message was sent.
        - I was originally using the ``user_id`` for the ``To`` field, but this
          query includes sent *and* received messages.

        """
        result = False
        email_date = from_email = message = subject = to_email = None
        message_data = (
            service.users().messages().get(userId=user_id, id=msg_id).execute()
        )
        # labels = message_data["labelIds"]
        payload = message_data["payload"]
        mime_type = payload["mimeType"]
        # https://stackoverflow.com/questions/21787496/converting-epoch-time-with-milliseconds-to-datetime
        internal_date = message_data["internalDate"]
        seconds = int(internal_date) / 1000.0
        email_date = datetime.fromtimestamp(seconds, tz=pytz.utc)
        headers = payload["headers"]
        for x in headers:
            name = x["name"]
            value = x["value"]
            if name == "From":
                ignore_from_name, from_email = parseaddr(value)
            elif name == "Subject":
                subject = value
            if name == "To":
                ignore_from_name, to_email = parseaddr(value)
        message = self._strip_multi_newlines(self._message(mime_type, payload))
        if not message:
            raise GoogleError("email '{}' has no message".format(msg_id))
        if not to_email:
            to_email = user_id
        # should we ignore mail from this address?
        if not EmailAddressToIgnore.objects.ignore(from_email):
            self.init_google_mail(
                msg_id, from_email, to_email, email_date, subject, message
            )
            result = True
        return result

    def _message(self, mime_type, payload):
        result = ""
        if mime_type in ("text/plain", "text/html"):
            data = payload["body"]["data"]
            result = self._message_as_text(mime_type, data)
        elif mime_type in (
            "multipart/alternative",
            "multipart/mixed",
            "multipart/related",
        ):
            parts = payload["parts"]
            result = self._message_from_parts(parts)
            if not result:
                # look for more 'parts' in the part
                # https://stackoverflow.com/questions/37445865/gmail-api-where-to-find-body-of-email-depending-of-mimetype
                for part in parts:
                    if "parts" in part:
                        result = self._message_from_parts(part["parts"])
                        if result:
                            break
        else:
            logger.error("Unknown mime type in payload: {}".format(mime_type))
        return result

    def _message_from_parts(self, parts):
        result = ""
        for part in parts:
            mime_type = part["mimeType"]
            if mime_type == "text/plain":
                data = part["body"]["data"]
                result = self._message_as_text(mime_type, data)
                break
            elif mime_type == "text/html":
                data = part["body"]["data"]
                result = self._message_as_text(mime_type, data)
            else:
                logger.error("Unknown mime type in part: {}".format(mime_type))
        return result

    def _message_as_text(self, mime_type, data):
        result = ""
        s = base64.urlsafe_b64decode(data.encode("ASCII"))
        if mime_type == "text/plain":
            result = s.decode("utf-8")
        elif mime_type == "text/html":
            soup = BeautifulSoup(s, "html.parser")
            result = soup.get_text("\n")
        else:
            raise GoogleError("Unknown mime type: {}".format(mime_type))
        return result

    def _messages(self, service, user_id):
        count = 0
        response = (
            service.users()
            .messages()
            .list(userId=user_id, maxResults=99, q="newer_than:1d -is:chat")
            .execute()
        )
        if "messages" in response:
            messages = response["messages"]
            for x in messages:
                msg_id = x["id"]
                # check to see if we already downloaded the message
                if not self.msg_id_exists(msg_id):
                    if self._mail(service, user_id, msg_id):
                        count = count + 1
        return count

    def _service_account(self, user_id):
        """Create credentials (or something) to access the Google account."""
        credentials = service_account.Credentials.from_service_account_file(
            settings.SERVICE_ACCOUNT_FILE, scopes=self.SCOPES
        )
        return credentials.with_subject(user_id)

    def _service_admin(self, credentials):
        return googleapiclient.discovery.build(
            "admin", "directory_v1", credentials=credentials
        )

    def _service_gmail(self, credentials):
        return googleapiclient.discovery.build(
            "gmail", "v1", credentials=credentials
        )

    def _strip_multi_newlines(self, message):
        count = 0
        result = []
        for line in message.split("\n"):
            if len(line.strip()) == 0:
                count = count + 1
            else:
                count = 0
            if count > 1:
                pass
            else:
                result.append(line)
        return "\n".join(result)

    def create_google_mail(
        self, msg_id, from_email, to_email, email_date, subject, message
    ):
        x = self.model(
            msg_id=msg_id,
            from_email=from_email,
            to_email=to_email,
            email_date=email_date,
            subject=subject,
            message=message,
        )
        x.save()
        return x

    def init_google_mail(
        self, msg_id, from_email, to_email, email_date, subject, message
    ):
        try:
            x = self.model.objects.get(msg_id=msg_id)
            if not x.from_email == from_email:
                raise GoogleError(
                    "from email does not match '{}' != '{}' ('{}'}".format(
                        x.from_email, from_email, subject
                    )
                )
            if not x.to_email == to_email:
                raise GoogleError(
                    "to email does not match '{}' != '{}' ('{}'}".format(
                        x.to_email, to_email, subject
                    )
                )
        except self.model.DoesNotExist:
            x = self.create_google_mail(
                msg_id, from_email, to_email, email_date, subject, message
            )
        return x

    def msg_id_exists(self, msg_id):
        result = False
        try:
            self.model.objects.get(msg_id=msg_id)
            result = True
        except self.model.DoesNotExist:
            pass
        return result

    def sync(self):
        count = 0
        domain_emails = self._domain_emails()
        for email in domain_emails:
            credentials = self._service_account(email)
            service = self._service_gmail(credentials)
            count = count + self._messages(service, email)
        return count


class GoogleMail(models.Model):
    msg_id = models.CharField(max_length=255, unique=True)
    email_date = models.DateTimeField()
    from_email = models.EmailField()
    to_email = models.EmailField()
    subject = models.TextField()
    message = models.TextField()
    objects = GoogleMailManager()

    class Meta:
        ordering = ["-email_date", "msg_id"]
        verbose_name = "Google Mail"
        verbose_name_plural = "Google Mail"

    def __str__(self):
        return "{}".format(self.msg_id)

    def summary(self):
        result = []
        for line in self.message.split("\n"):
            if len(result) > 3:
                result.append("...")
                break
            line = line.strip()
            if len(line) == 0:
                pass
            elif "dear" in line.lower():
                pass
            else:
                result.append(line)
        return "\n".join(result)
