#!/bin/bash
# treat unset variables as an error when substituting.
set -u

# pip install -r requirements/local.txt

DATABASE=dev_googl

# drop the databases - if it exists
psql -X -U postgres -c "DROP DATABASE ${DATABASE}"

# exit immediately if a command exits with a nonzero exit status.
set -e

psql -X -U postgres -c "CREATE DATABASE ${DATABASE} TEMPLATE=template0 ENCODING='utf-8';"

echo Deleting files in 'media-private'
rm -rf ./media-private/

echo Deleting log file
touch logger.log
rm logger.log

django-admin.py migrate --noinput
django-admin.py init_project
django-admin.py demo_data_project
django-admin.py demo_data_login
django-admin.py init_app_google

django-admin.py runserver $@
