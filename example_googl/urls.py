# -*- encoding: utf-8 -*-
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, re_path

from .views import DashView, HomeView, SettingsView


urlpatterns = [
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^dash/$", view=DashView.as_view(), name="project.dash"),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^", view=include("login.urls")),
    re_path(r"^google/", view=include("googl.urls")),
]

urlpatterns += staticfiles_urlpatterns()

if settings.ALLOW_DEBUG_TOOLBAR:
    import debug_toolbar

    urlpatterns += [re_path(r"^__debug__/", include(debug_toolbar.urls))]
