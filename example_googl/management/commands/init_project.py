# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand


class Command(BaseCommand):

    help = "init 'googl' app"

    def handle(self, *args, **options):
        self.stdout.write("{} - Complete".format(self.help))
